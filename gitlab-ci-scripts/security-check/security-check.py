import json
import os
import sys
from colorama import Fore, Back, Style

path = '.'
exclude_paths = './tests,./gitlab-ci-scripts'
output = os.popen(f'bandit -q -r -f json -x {exclude_paths} {path}').read()
output_dict = json.loads(output)
exit_status = 0
lows = 0
for result in output_dict['results']:
    print(Fore.RED + 'Security issue found!')
    print(Fore.YELLOW)
    print('Test ID:', result['test_id'])
    print('Severity:', result['issue_severity'], 'in', result['filename'])
    print(result['issue_text'])
    print('Line:', result['line_number'])
    print(Style.RESET_ALL)
    issue_severity = result['issue_severity']
    if issue_severity == 'MEDIUM' or issue_severity == 'HIGH':
        exit_status = 1
    elif issue_severity == 'LOW':
        lows += 1
if lows >= 30:
    exit_status = 1

if exit_status == 0:
    print(Fore.GREEN)
    print('No security issues found.')
    print(Style.RESET_ALL)

sys.exit(exit_status)
