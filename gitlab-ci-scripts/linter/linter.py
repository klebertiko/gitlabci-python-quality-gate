import json
import os
import sys

from colorama import Fore, Style

paths = 'src'
output = os.popen(f'pylint --rcfile gitlab-ci-scripts/linter/.pylint.rc -f json {paths}').read()
output_dict = json.loads(output)
exit_status = 0
for issue in output_dict:
    issue_type = issue["type"]
    if issue_type != 'convention' and issue_type != 'warning':
        print(Fore.RED + 'Linter issue found!')
        print(Fore.YELLOW)
        print(f'Message ID: {issue["message-id"]}')
        print(f'Message: {issue["message"]}')
        print(f'Type: {issue["type"]}')
        print(f'Path: {issue["path"]}')
        print(f'Line: {issue["line"]}')
        print(f'Column: {issue["column"]}')
        print(Style.RESET_ALL)
        exit_status = 1

if exit_status == 0:
    print(Fore.GREEN)
    print('No linter issues found.')
    print(Style.RESET_ALL)

sys.exit(exit_status)