import src.validate_json_schema as validate_json_schema


def test_validate_valid_json_valid_schema():
    """
    Validate JSON schema
    Returns True when schema and instance are validated
    >>> instance = {"name": "foo"}
    >>> schema = {"type": "object", "properties": {"name": {"type": "string"}, }, }
    >>> validate_json_schema.validate_schema(instance, schema)
    >>> True
    """
    schema = {
        "type": "object",
        "properties": {
            "name": {"type": "string"},
        },
    }
    assert validate_json_schema.validate_schema({"name": "foo"}, schema)


def test_validate_valid_json_invalid_event():
    """Validate JSON schema
    Returns False when schema and instance are not validated
    >>> instance = {"name": 1}
    >>> invalid_event = {"type": "object", "properties": {"name": {"type": "string"}, }, }
    >>> validate_json_schema.validate_schema(instance, invalid_event)
    >>> False
    """
    invalid_event = {
        "type": "object",
        "properties": {
            "name": {"type": "string"},
        },
    }
    assert not validate_json_schema.validate_schema({"name": 1}, invalid_event)


def test_validate_invalid_json_valid_schema():
    """Validate JSON schema
    Returns False when schema and instance are not validated
    >>> instance = {"number": 1}
    >>> schema = {"type": "object", "properties": {"name": {"type": "string"}, "number": {"type": "string"}, }, }
    >>> validate_json_schema.validate_schema(instance, schema)
    >>> False
    """
    schema = {
        "type": "object",
        "properties": {
            "name": {"type": "string"},
            "number": {"type": "string"},
        },
    }
    assert not validate_json_schema.validate_schema({"number": 1, "name": ""}, schema)


def test_validate_invalid_json_field_value_valid_schema():
    schema = {
        "$schema": "http://json-schema.org/draft-07/schema",
        "$id": "http://example.com/pos.schema.json",
        "type": "object",
        "title": "{\"partition\" :[\"event_type\", \"dt\"]}",
        "description": "Header Schema - Aliança POS",
        "required": [
            "timestamp"
        ],
        "properties": {
            "timestamp": {
                "$id": "#/properties/timestamp",
                "type": "string",
                "format": "date-time",
                "title": "",
                "description": "Data em que o evento foi gerado em formato UTC",
                "examples": [
                    "2020-04-16T14:13:10.456Z"
                ]
            }
        }
    }

    event = {"timestamp": "2020-09-29T12:48:00z"}
    assert validate_json_schema.validate_schema(event, schema)
